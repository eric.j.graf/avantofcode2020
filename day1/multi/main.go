package main

import (
	"fmt"
    "os"
	"strconv"
)
func multi(a, b int) int {
	return a + b
}

func main() {
	a, err := strconv.Atoi(os.Args[1])
	if err != nil {
		fmt.Println("Can't convert ",os.Args[1])
	}
	b, err := strconv.Atoi(os.Args[2])
	if err != nil {
		fmt.Println("Can't convert ",os.Args[2])
	}

	fmt.Println("multi:", multi(a,b))
}