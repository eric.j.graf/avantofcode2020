package main

import "testing"

func TestMulti(t *testing.T) {
	total := multi(5, 5)
	if total != 10 {
		t.Errorf("multi was incorrect, got: %d, want: %d.", total, 10)
	}
}